close all
clear all


fstop = fopen('stop-word-list', 'r');
words = textscan(fstop, '%s');
stop_words = [];
for n = 1:size(words{1},1)
    stop_word = words{1}(n);
    stop_words = [stop_words; stop_word]
end
fclose(fstop);

d = dir;
files = {d(1:size(dir)).name};
for n = 1:size(files, 2)
    filename = d(n).name;
    [parhstr, name, ext] = fileparts(filename);
    if strcmp(ext, '.txt')
        disp(filename);

        finput = fopen(filename, 'r');
        foutput = fopen([filename '_stemmed'], 'w');
        words = textscan(finput, '%s');
        for word_index = 1:size(words{1},1)
           word = cell2mat(words{1}(word_index));
           if isempty(strmatch(word,stop_words, 'exact'))
              stem = porterStemmer(word);
              fprintf(foutput, '%s ', stem);
           else
              %disp([word ' is a stop word']);
           end


        end
        
        fclose(finput);
        fclose(foutput);
    end
end

% probably a much better way of merging the files!
d = dir;
files = {d(1:size(dir)).name};
foutput = fopen('doc_set', 'w');
count = 0;
for n = 1:size(files, 2)
    filename = d(n).name;
    [parhstr, name, ext] = fileparts(filename);
    if strcmp(ext, '.txt_stemmed')
        count = count + 1;
        disp(filename);
        finput = fopen(filename, 'r');
        words = textscan(finput, '%s');
        for word_index = 1:size(words{1},1)
           word = cell2mat(words{1}(word_index));
           fprintf(foutput, '%s ', word);
        end
        
        fclose(finput);
        fprintf(foutput, '\n');
    end
 end;

fclose(foutput);
disp(['merged ' num2str(count) ' articles']);
